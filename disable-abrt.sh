#!/usr/bin/env bash

if [[ $(id -u) -ne 0 ]] ; then
    echo "Please run this script as root"
    exit 1
fi

systemctl stop abrt-journal-core.service
systemctl disable abrt-journal-core.service
systemctl stop abrt-oops.service
systemctl disable abrt-oops.service
systemctl stop abrt-xorg.service
systemctl disable abrt-xorg.service
systemctl stop abrtd.service
systemctl disable abrtd.service
