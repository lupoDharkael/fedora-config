# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
export PS1="\[\033[38;5;39m\][\[$(tput bold)\]\[$(tput sgr0)\]\[\033[38;5;46m\]\u\[$(tput sgr0)\]\[$(tput sgr0)\]\[\033[38;5;39m\]]\w:\[$(tput sgr0)\]\[\033[38;5;15m\] \[$(tput sgr0)\]"

alias l='ls -GghB --group-directories-first'
alias ll='ls -hAl --group-directories-first'
alias gits='git status'
alias gitd='git diff'
alias gitds='git diff --shortstat'
alias update='sudo dnf update -y && flatpak update -y && flatpak -y uninstall --unused'
alias gitlog='git log --stat --pretty=short --graph'
alias gitfuck='git commit --amend --no-edit'
alias gbd='godot-build-debug'
alias gb='godot-build'
