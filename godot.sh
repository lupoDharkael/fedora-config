#!/usr/bin/env bash

echo "------ Configuring Godot ------"
install godot/godot.desktop /usr/share/applications
install godot/godot.svg /usr/share/icons/hicolor/scalable/apps

GODOT="Godot_v3.1.2-stable_x11.64"
wget https://downloads.tuxfamily.org/godotengine/3.1.2/$GODOT.zip &&
unzip $GODOT.zip &&
rm $GODOT.zip &&
mv $GODOT /usr/local/bin/godot