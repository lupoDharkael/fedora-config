#!/usr/bin/env bash

if [[ $(id -u) -ne 0 ]] ; then
    echo "Please run this script as root"
    exit 1
fi

sudo -E -u $SUDO_USER mkdir /home/$SUDO_USER/.kvm &&
virsh pool-destroy default &&
virsh pool-undefine default &&
virsh pool-define-as --name default --type dir --target /home/$SUDO_USER/.kvm &&
virsh pool-autostart default &&
virsh pool-start default
