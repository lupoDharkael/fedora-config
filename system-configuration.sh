#!/usr/bin/env bash

if [[ $(id -u) -ne 0 ]] ; then
    echo "Please run this script as root"
    exit 1
fi

echo "------ Installing Rpmfusion ------"
dnf install -y https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm

dnf update -y

dnf install -y vim-default-editor --allowerasing

# flatpak
echo "------ Installing Flatpak ------"
dnf install -y flatpak &&
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak install -y flathub org.telegram.desktop
flatpak install -y flathub com.github.libresprite.LibreSprite
flatpak install -y com.github.tchx84.Flatseal
flatpak install -y flathub com.github.marktext.marktext
flatpak install -y flathub org.olivevideoeditor.Olive
flatpak install -y flathub com.jgraph.drawio.desktop
flatpak install -y flathub com.discordapp.Discord
flatpak install -y flathub net.ankiweb.Anki
flatpak install -y flathub io.github.seadve.Kooha

flatpak override --filesystem=host org.telegram.desktop

# uninstall
echo "------ Uninstalls ------"
dnf remove -y kwrite dragon konqueror

# ffmpeg
dnf install -y ffmpeg ffmpeg-libs --allowerasing

# install
echo "------ Installs ------"
DEV="vulkan-validation-layers yasm vim clang clang-tools-extra lld scons gcc-c++ valgrind tig cppcheck cmake git kile kompare krename libstdc++-static opencv opencv-devel libomp-devel"
SYS_TOOLS="flameshot pngquant ktorrent corectrl lm_sensors grub-customizer wget htop wireshark kate skanlite @virtualization"
MULTIMEDIA="vlc krita obs-studio gimp inkscape youtube-dl audacity frei0r-plugins"
GAMES="goverlay dolphin-emu blender gamemode lutris wine"
OTHER="ckb-next libreoffice breeze-gtk plymouth-theme-breeze wine-desktop wine-fonts"

# TODO install Syng

dnf install -y $DEV $SYS_TOOLS $MULTIMEDIA $GAMES $OTHER

dnf builddep -y supertuxkart

# Rust
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# vscodium
# manual: https://github.com/VSCodium/vscodium/releases
sudo tee -a /etc/zypp/repos.d/vscodium.repo << 'EOF'
[gitlab.com_paulcarroty_vscodium_repo]
name=gitlab.com_paulcarroty_vscodium_repo
baseurl=https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/rpms/
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg
metadata_expire=1h
EOF

sudo dnf install codium

codium --install-extension bungcip.better-toml
codium --install-extension rust-lang.rust-analyzer
codium --install-extension vadimcn.vscode-lldb
codium --install-extension llvm-vs-code-extensions.vscode-clangd
codium --install-extension ms-python.python

# steam
dnf install -y fedora-workstation-repositories
dnf install -y steam --enablerepo=rpmfusion-nonfree-steam

# dotfiles
sudo -E -u $SUDO_USER install dotfiles/.bashrc /home/$SUDO_USER/
sudo -E -u $SUDO_USER install dotfiles/.gitconfig /home/$SUDO_USER/
# .mozilla/firefox
# .local/share/

# shortcuts
# firefox bookmarks
# dark theme
# no border window

sed -i 's/StartServer=true/StartServer=false/g' /home/$SUDO_USER/.config/akonadi/akonadiserverrc

# commands
echo "------ Adding Custom Commands ------"
install bin/findtext /usr/local/bin
install bin/youtube-dl-music /usr/local/bin

# konsole Configuration
# Ctrl-Shift-E: will split the view vertically.
# Ctrl-Shift-O: will split the view horizontally.
# Ctrl-Shift-P: will focus be active on the previous view.
# Ctrl-Shift-N: will focus be active on the next view.
# Ctrl-Shift-W: will close the view where the focus is on.

# Breeze-gtk download and apply.

plymouth-set-default-theme breeze

updatedb

sudo -E -u $SUDO_USER notify-send -i appointment -u critical "Configuration ready"
