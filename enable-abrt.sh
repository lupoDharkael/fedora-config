#!/usr/bin/env bash

if [[ $(id -u) -ne 0 ]] ; then
    echo "Please run this script as root"
    exit 1
fi

systemctl start abrt-journal-core.service
systemctl enable abrt-journal-core.service
systemctl start abrt-oops.service
systemctl enable abrt-oops.service
systemctl start abrt-xorg.service
systemctl enable abrt-xorg.service
systemctl start abrtd.service
systemctl enable abrtd.service
